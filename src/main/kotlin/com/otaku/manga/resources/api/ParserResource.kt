package com.otaku.manga.resources.api

import com.otaku.manga.models.MangaModel
import com.otaku.manga.repositories.MangaRepository
import org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class ParserResource(private val mangaRepository: MangaRepository) {

    @PostMapping("/api/parser", produces = [APPLICATION_JSON_UTF8_VALUE])
    fun getMangaFromParser(@RequestBody(required = true) manga: MangaModel): ResponseEntity<String> {

        mangaRepository.save(manga)
        return ResponseEntity.ok("okey")
    }
}
