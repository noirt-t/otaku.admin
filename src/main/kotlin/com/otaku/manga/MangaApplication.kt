package com.otaku.manga

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MangaApplication

fun main(args: Array<String>) {
    runApplication<MangaApplication>(*args)
}

