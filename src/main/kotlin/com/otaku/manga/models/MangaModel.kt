package com.otaku.manga.models

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "Mangas")
class MangaModel(
    val name: String,
    val anotherName: List<String>,
    val type: String,
    val authtor: List<String>,
    val status: String,
    val downloaded: String,
    val tags: List<String>,
    val translators: List<String>,
    val mangaUri: String,
    val similarManga: List<String>,
    val description: String,
    val chapters: List<ChapterModel>
) {
    @Id
    private lateinit var Id: String
}
