package com.otaku.manga.models

class PageModel(
    val id: Int,
    val pageUri: String
)
