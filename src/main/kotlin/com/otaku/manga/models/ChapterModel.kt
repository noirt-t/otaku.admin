package com.otaku.manga.models

class ChapterModel(
    val id: Int,
    val name: String,
    val chupterPageUrl: String,
    val downloadDate: String, // TODO: replace on Date object
    val pages: List<PageModel>
)
