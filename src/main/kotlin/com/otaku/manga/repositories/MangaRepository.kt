package com.otaku.manga.repositories

import com.otaku.manga.models.MangaModel
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface MangaRepository : MongoRepository<MangaModel, String> {
    fun save(manga: String)
}
